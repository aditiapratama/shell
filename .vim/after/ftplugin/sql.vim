iab qi( quote_ident(
iab ql( quote_literal(
iab ai auto_increment
iab ak bigserial PRIMARY KEY
iab at ALTER TABLE
iab crf CREATE OR REPLACE FUNCTION
iab cri CREATE INDEX
iab crs CREATE SEQUENCE
iab crt CREATE TABLE
iab crtrig create or replace functionmqa() returns trigger as$body$declarebeginend;$body$language 'plpgsql';create trigger  after before  on  for each row execute procedure ();`qa
iab crtt CREATE TEMP TABLE
iab crui CREATE UNIQUE INDEX
iab crv CREATE VIEW
iab def DEFAULT
iab fkey FOREIGN KEY
iab gd GET DIAGNOSTICS = row_count;bbhi
iab i4 int4
iab i8 int8
iab ii INSERT INTO
iab inf IF NOT FOUND
iab ise bveyais null or pa = ''''
iab lg language
iab nf NOT FOUND
iab nn NOT NULL
iab nv nextval
iab ob ORDER BY
iab odc ON DELETE CASCADE
iab odn ON DELETE SET NULL
iab pkey PRIMARY KEY
iab refs REFERENCES
iab ret RETURN
iab rets RETURNS
iab rtf RETURN false;
iab seq SEQUENCE

map <silent> <F1>	:set noaiV:!perl -ne 'chomp;s/\s*,\s*/,/g;s/\s+/ /;@a=split;$a[2]=uc($a[2]);@b=map{uc}split(/,/,$a[1]);printf("-- Function name: \%s\012-- Description  : \012-- Parameters:\012",$a[0]);for $c(0..$\#b){printf("-- \%2u. \%-8s :\n",$c+1,$b[$c])}printf("-- Returns:\012--     \%-8s : \012",$a[2]);printf("CREATE OR REPLACE FUNCTION \%s(\%s) RETURNS \%s AS \047\012DECLARE\012", $a[0], join(", ",@b), $a[2]);for $c (0..$\#b){printf("	in_                  ALIAS FOR \$\%u;\012",$c+1)}print"BEGIN\012END;\012\047 LANGUAGE \047plpgsql\047;\012"':set ai
map <silent> <F2>	VyPV:s/^CREATE\+\s\+\(\S\+\s\+[^ ;(]\+\(([^)]\+)\)\=\).*$/DROP \1;/i/qwdfqdq

" tworzenie tabel ze string�w typu: tabela pole1 typ1, pole2 typ2
"map <silent> <F10>	V:!~/.vim/addons/sql-createTable<Return>
"vmap <silent> <F10>	:!~/.vim/addons/sql-createTable<Return>

" wstawianie indeks�w unikalnych (ctrl-x) i zwyk�ych <alt-x>
map <silent> <C-x>	mf^vE"fy?^CREATE TABLE<Return>2WvE"ty/^)<Return>oCREATE UNIQUE INDEX ui_<C-r>t_<C-r>f ON <C-r>t (<C-r>f);<Esc>`f
map <silent> <M-x>	mf^vE"fy?^CREATE TABLE<Return>2WvE"ty/^)<Return>oCREATE INDEX i_<C-r>t_<C-r>f ON <C-r>t (<C-r>f);<Esc>`f

" wstawianie/usuwanie NOT NULL
map <silent> <C-n>	V:!~/.vim/addons/sql-notNull<Return>
vmap <silent> <C-n>	:!~/.vim/addons/sql-notNull<Return>

" wstawianie FOREIGN KEY
map <silent> <C-f>	mf^vE"fy?^CREATE TABLE<Return>2wvE"ty/^)<Return>oALTER TABLE <C-r>t ADD FOREIGN KEY (<C-r>f) REFERENCES <C-r>f<Esc>:s/_id$//<Enter>:s/s$/se/e<Enter>As (id);<Esc>`f

set expandtab
