set   expandtab
set   shiftwidth=2
set   smartindent
set   smarttab
set   softtabstop=2
set   tabstop=2
set   textwidth=98
