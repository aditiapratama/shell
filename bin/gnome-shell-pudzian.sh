#!/bin/bash
# Work around gnome-shell memory leaks.

# Thresholds:
mem_max_pct="12.5"
mem_max_kb="500000"

debug () {
    echo "$(basename $0) $$ $(date) $@"
}

meminfo () {
    perl -pe 's{(\d+) kB}{ sprintf "%.1f GiB", $1/1024/1024 }e' /proc/meminfo
}

do_restart=""
pids="$(pgrep -u $LOGNAME gnome-shell)"

for pid in $pids; do
    # snatch env var required for busctl (DBus)
    eval export $(strings /proc/$pid/environ | grep DBUS_SESSION_BUS_ADDRESS)
    mem_pct=$(ps -p $pid -o '%mem=')
    if [ $(echo "$mem_pct >= $mem_max_pct" | bc) -eq 1 ]; then
        debug "Scheduling restart because gnome-shell $pid uses $mem_pct% >= $mem_max_pct% of memory."
        do_restart=yes
        break
    fi
    mem_kb=$(ps -p $pid -o 'rss=')
    if [ $(echo "$mem_kb >= $mem_max_kb" | bc) -eq 1 ]; then
        debug "Scheduling restart because gnome-shell $pid uses $mem_kb KB >= $mem_max_kb KB of memory."
        do_restart=yes
        break
    fi
done

if [ "$do_restart" ]; then
    debug "Memory before:"
    free -htw
    debug "Executing Meta.restart()."
    busctl --user call org.gnome.Shell /org/gnome/Shell org.gnome.Shell \
        Eval s 'Meta.restart("Restarting...")' >/dev/null
    sleep 15
    debug "Memory after:"
    free -htw
else
    debug "Restart was not needed."
fi
