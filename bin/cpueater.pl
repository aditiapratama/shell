#!/usr/bin/perl -w
use v5.18;
use Time::HiRes qw(time);
use List::Util qw(sum);
my $TSIZE = 10;

my $duration;
my $duration_loop;
my $every = 2000000;
my $index = 0;
my @table = (1) x $TSIZE;
my $t0    = time;
my $t1    = $t0;

while (1) {

	$table[ $index % $TSIZE ] = 0.5 + rand() * $table[ ( $index + 1 ) % $TSIZE ];
	
	my $junk = sum @table;

	if ( $index % $every == 1 ) {

		my $duration = time - $t0;
		my $lps      = int( $index / $duration );

		my $duration_loop = time - $t1;
		my $lps_loop      = int( $every / $duration_loop );

		my $cpu = `ps -o pcpu= $$`;
		chomp $cpu;

		say "LPS = $lps; LPS now = $lps_loop; pcpu = $cpu";

		$t1 = time;
	} ## end if ( $index % $every ==...)
	$index++;
} ## end while (1)
