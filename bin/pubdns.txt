nameserver 1.1.1.1 # Cloudflare
nameserver 1.1.1.2 # Cloudflare
nameserver 1.1.1.3 # Cloudflare
nameserver 8.8.8.8 # Google
nameserver 8.8.4.4 # Google
nameserver 208.67.222.222 # Cisco (USA)
nameserver 208.67.220.220 # Cisco (USA)
nameserver 84.200.69.80	# DNS.Watch (DE)
nameserver 84.200.70.40	# DNS.Watch (DE)
nameserver 64.6.64.6 # Verisign
nameserver 64.6.65.6 # Verisign
