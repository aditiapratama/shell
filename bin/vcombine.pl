#!/usr/bin/env perl
use v5.20;
use List::Util qw(max pairs);

# Unicode symbols for group/record/header separator
my ($group_sep, $record_sep, $header_sep) = ("", "", " ");

sub usage {
    return qq{
Combine text files vertically.
Usage:
    vcombine ( NAME FILE ) ...
Example:
    vcombine "ls -l" /tmp/tmpf323d182 "uptime" /tmp/tmpfbtpb2l1
Names should be unique.};
}

my @INPUT = pairs @ARGV;    # AoA

unless (@INPUT) {
    say STDERR usage();
    exit 1;
}

my @RESULT;                 # AoH

for my $item (@INPUT) {
    my $name = $item->key;
    my $file = $item->value;
    open my $fd, $file or die "Can't open \"$file\": $!";
    my $width = length $name;
    my @result;
    while (<$fd>) {
        chomp;
        $width = length if $width < length;
        push @result, $_;
    }
    push @RESULT, {name => $name, width => $width, line => \@result};
}

my @output     = ();                                         # AoA
my $max_length = max map { scalar @{$_->{line}} } @RESULT;
for my $line_no (0 .. $max_length - 1) {
    my @vlines = map { $_->{line}[$line_no] } @RESULT;
    push @output, \@vlines;
}

# add header:
unshift @output, [ map { $header_sep x $_->{width} } @RESULT ];
unshift @output, [ map { $_->{name} } @RESULT ];

for my $resultgroup (@output) {
    my @outputline = ();
    for my $i (0 .. scalar @$resultgroup - 1) {
        push @outputline, sprintf "%-$RESULT[$i]{width}s", $resultgroup->[$i];
    }
    say join $group_sep, @outputline;
}

