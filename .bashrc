# ~/.bashrc: executed by bash(1) for non-login shells.
# When  an  interactive  shell that is not a login shell is started, bash
# reads and executes commands from /etc/bash.bashrc and ~/.bashrc,
# if these files exist.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=20000
export HISTFILESIZE=100000
export HISTTIMEFORMAT="%F %R "

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f ~/.bash_functions ] ; then
    . ~/.bash_functions
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


export TERM=xterm-color

# Don't wait for job termination notification
# set -o notify

# Use case-insensitive filename globbing
shopt -s nocaseglob

# When changing directory small typos can be ignored by bash
# for example, cd /vr/lgo/apaache would find /var/log/apache
shopt -s cdspell

### LOCALE ###

# set base locale
if test "$OSTYPE" == "cygwin" -o "$TERM" == "cygwin"; then
    export LC_ALL=en_US
else
    export LC_ALL=en_US.UTF-8
fi
# set some variables to POSIX
export LC_NUMERIC=POSIX
export LC_TIME=POSIX
export LC_PAPER=POSIX
export LC_MEASUREMENT=POSIX

# PERL #
for libdir in ~/perl-libs ~/lib ~/perl ~/wfc-core/lib ~/cvs/perl-libs /u/w2gi/lib; do
    if test -d "$libdir"; then
        export PERL5LIB="$PERL5LIB:$libdir"
    fi
done
# userperl settings
if [ -d "$HOME/perl5" ]; then
    export PATH="$HOME/perl5/bin${PATH+:}${PATH}"
    export PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"
    export PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"
    export PERL_MB_OPT="--install_base \"$HOME/perl5\""
    export PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"
fi

# use GPG for SSH auth
# if [ -f "${HOME}/.gpg-agent-info" ]; then
#     source "${HOME}/.gpg-agent-info"
#     export GPG_AGENT_INFO
#     export SSH_AUTH_SOCK
#     export SSH_AGENT_PID
# else
#     eval $(gpg-agent --daemon)
# fi


# EDITOR/PAGER

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
export EDITOR=vim
export VISUAL=vim
#export EDITOR=joe
#export EDITOR=nano
#export EDITOR=mcedit

export PAGER=less
export LESS="--ignore-case --LONG-PROMPT --chop-long-lines --tabs=4 --quit-if-one-screen --no-init --RAW-CONTROL-CHARS"
### "--quit-at-eof --shift=0.25 --hilite-search --max-forw-scroll=1000 --max-back-scroll=1000"

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

if test `id -u` == "0" && test "$OSTYPE" != "cygwin" && test "$TERM" != "cygwin"; then
    TMOUT=$[60*90]
else
    unset TMOUT
fi
export TMOUT

# C/C++ compiler flags
#export CFLAGS="-O2 -march=athlon-xp"
#export CXXFLAGS=$CFLAGS
#export HOSTCFLAGS="-O2 -march=i686"
#export HOSTCXXFLAGS=$HOSTCFLAGS

# vars for some old backup tools
#export TAPE='/dev/nst0'
#export SYBASE='/usr/local/freetds'
#export BXSHARE='/usr/local/share/bochs'

export LOGPATHS="/var/log"
# could also
#export LOGPATHS="$LOGPATHS /service/*/log" # for daemontools
#export LOGPATHS="$LOGPATHS /var/lib/mysql/*.log /var/lib/mysql/*.err" # for MySQL
#export LOGPATHS="$LOGPATHS /usr/local/squid/var/logs" # for squid cache
#export LOGPATHS="$LOGPATHS /tmp/amanda" # for Amanda

# per-user locatedb
if test -r "$HOME/.locatedb"; then
    export LOCATE_PATH="$HOME/.locatedb"
fi

# silence bc
export BC_ENV_ARGS=--quiet

### set java path if Sun java present
if test -d "/usr/lib/jvm/java-6-sun"; then
    export JAVA_HOME="/usr/lib/jvm/java-6-sun"
    export PATH=$PATH:$JAVA_HOME/bin
fi

# set PATH so it includes user's private bin if it exists
for bd in ~/bin ~/.local/bin; do
    if [ -d $bd ]; then
        export PATH="$PATH:$bd"
    fi
done

# do the same with MANPATH
if [ -d ~/man ]; then
    export MANPATH="${MANPATH}":~/man
fi

if [ -f $HOME/.bash_pg_variables ]; then
    source $HOME/.bash_pg_variables
fi

# always run ssh-agent
#if ! test "$SSH_AGENT_PID" || ! kill -0 $SSH_AGENT_PID ; then
#    eval `ssh-agent`
#    ssh-add
#fi

# Neo4j env vars
# export NEO4J_HOME=/home/neo4j/neo4j

# TeX Live env vars
if [ -d "$HOME/Aplikacje/texlive/2018" ]; then
    export TEXLIVE="$HOME/Aplikacje/texlive/2018"
    export MANPATH="$MANPATH:$TEXLIVE/texmf-dist/doc/man"
    export INFOPATH="$INFOPATH:$TEXLIVE/texmf-dist/doc/info"
    export PATH="$PATH:$TEXLIVE/bin/x86_64-linux"
fi

# Completions

# Google cloud SDK
GCSDK="$HOME/Aplikacje/google-cloud-sdk"
if [ -d "$GCSDK" ]; then
  # The next line updates PATH for the Google Cloud SDK.
  if [ -f "$GCSDK/path.bash.inc" ]; then . "$GCSDK/path.bash.inc"; fi
  # The next line enables shell command completion for gcloud.
  if [ -f "$GCSDK/completion.bash.inc" ]; then . "$GCSDK/completion.bash.inc"; fi
fi

export PS1="\u@\h:\w\$ "

if type kubectl &>/dev/null && ! type __start_kubectl &>/dev/null; then
    source <(kubectl completion bash)
    export PS1="\$(kubectl config current-context 2>/dev/null) $PS1"
fi

if type gh &>/dev/null && ! type __start_gh &>/dev/null; then
    source <(gh completion --shell bash)
fi

if type pip &>/dev/null && ! type -t _pip_completion &>/dev/null; then
    source <(pip completion --bash)
fi

# Custom user settings
if [ -f ~/.bashrc_local ]; then
    source ~/.bashrc_local
fi
