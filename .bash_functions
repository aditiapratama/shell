#!/bin/bash

# some user-made functions

# aliass: ALIAS and Save
function aliass() {
	source ~/.bash_aliases || return
	alias "$@" >/dev/null || return
	local _alias=`echo "$@" | cut -d'=' -f1`
	local _value=`echo "$@" | cut -d'=' -f2`
	#echo "checking [$_alias] [$_value]"
	[ -z "$_alias" ] && return
	[ -z "$_value" ] && unalias $_alias
	local tmp=`mktemp`
	alias | sort > $tmp && mv $tmp ~/.bash_aliases
}

# avifind: find video files using FIND. usage: avifind /list/of/paths /to/search
function avifind() {
	find $@ -type f -iregex ".*\.\(avi\|flv\|mpg\|mpeg\|mp4\|mpg4\|mkv\|ogm\|rm\|wmv\)"
}

# bdif: Bazaar DIFf
function bdif() {
	bzr diff $* | vim -R -
}

# bdu: Better DU
function bdu() {
	find "$@" -mindepth 1 -maxdepth 1 -type d -exec du -s {} \+ | sort -n
}

# brol: find BROken Links
function brol() {
	find "$@" -type l -print0 | \
	xargs -r0 file | \
	grep "broken symbolic" | \
	sed -e 's/^\|: *broken symbolic.*$//g'
}

function clone_dir () {
  local srcdir=$1
  local destdir=$2
  debug "Cloning $destdir from $srcdir" DEBUG
  local t0=$(date +%s)
  rsync -au $srcdir/ $destdir/
  local dirtime=$(($(date +%s) - t0))
  local dirsize=$(sizeof $destdir)
  debug "Cloned $destdir from $srcdir (size: $dirsize KB, time: $dirtime s)" INFO
}

# cprompt: Coloured PROMPT
function cprompt() {
	local RED="\[\033[1;31m\]"
	local GREEN="\[\033[1;32m\]"
	local NO_COL="\[\033[0m\]"
	local WHITE="\[\033[1;37m\]"
	local BLUE="\[\033[1;34m\]"
    PS0time=0
    PS1calc=0
    # Note: both PS0 and PS1 need careful quoting to protect $(()) inside. It is subject to
    # expansion in bash runtime, not in this script.
    # PS1 is regular prompt and PS0 is displayed right after command entry, before execution.
    PS0='${PS1:$((PS0time=\D{%s}, PS1calc=1, 0)):0}'"$NO_COL[$GREEN\D{%-d/%-m/%y} \t$NO_COL]\n"
    PS1="$NO_COL[$GREEN\D{%-d/%-m/%y} \t$NO_COL]"' (e:$((PS1calc ? \D{%s}-$PS0time : 0)) s:${?:PS1calc=0})'"\n$GREEN\u$NO_COL@$GREEN\h$NO_COL:$BLUE\w$NO_COL $GREEN\$ $NO_COL" 
	PS2="$GREEN> $NO_COL"
}

function debug () {
# Wyświetl komunikat diagnostyczny
  local message=$1
  local level=${2:-DEBUG}
  local ts=$(date +'%F %T')
  local prog=$(basename -- $0)
  echo "$ts $prog $$ $level $message"
}

# ded: Delete Empty Directories
function ded() {
	find "$@" -depth -type d -empty -exec rmdir {} \+
}

# dempty: Delete Empty files and directories
function dempty() {
	find "$@" -depth -type f -size 0 -empty -exec rm -v {} \+
	find "$@" -depth -type d -empty -exec rmdir -v {} \+
}

# dirpar: For given file/dir, show all DIRectory PARents
function dirpar () {
	local dirs=$@
	local dir
	for dir in $dirs; do
		while [ $dir != $(dirname $dir) ]; do
			echo $dir;
			dir=$(dirname $dir);
		done
	done
}

# docfind aka oofind: FIND DOCuments
function docfind() {
	find $@ -type f -name '*README*' -o -name INSTALL \
		-o -iregex ".*\.\(f?od[tmps]\|\(doc\|xls\|ppt\)x?\|pdf\|rtf\|md\|txt\|html?\)"
}

# docgrep aka oogrep : DOCument contents GREP
function docgrep() {
	if test "$2"; then
		local pattern=$1
		shift 1
		local places=$@
	elif test "$1"; then
		local pattern=$1
		local places=.
	else
		echo "Usage: docgrep string_to_search /path/1 /path/2 ..."
		return 0
	fi
	docfind $places | while read file; do
		filename=$(basename "$file")
		extension="${filename##*.}"

		if unzip -p "$file" content.xml &>/dev/null ; then
			### OpenDocument files
			unzip -p "$file" content.xml | egrep -s -q -i "$pattern" && echo "$file"
		elif test "$extension" = "pdf"; then
			### pdf files
			pdftotext "$file" - | egrep -s -q -i "$pattern" &>/dev/null && echo "$file"
		else
			### other
			egrep -s -q -i "$pattern" < "$file" && echo "$file"
		fi
	done
}

# DOCKER INFOrmation / status
function dockerinfo () {
	echo +++ Docker ps +++; docker ps -a
	echo +++ Docker images +++; docker images
}

# dprompt: Debian PROMPT (suitable for chroots)
function dprompt() {
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
}

# dutop: calculate size of files/dirs and display top N
function dutop () {
	find "$@" -mindepth 1 -maxdepth 1 -exec du -s {} \; | sort -nr | head -n 20
}

# egw: Edit Git Working files
function egw () {
	local upstream=${1:-master}
	vim -o $(git diff $upstream --name-only)
}

# estimate_dnscache_motion: ESTIMATE daily DNSCACHE MOTION
function estimate_dnscache_motion() {
	[ -d /service/dnscache ] || return
	local last_stat=` grep "stats " /service/dnscache/log/main/current | tail -n 1 | awk '{print $4}'`
	local run_time=`svstat /service/dnscache | awk '{print $5}' `
	local dcm=$[ $last_stat * 3600 * 24 / $run_time ]
	local cs=`cat /service/dnscache/env/CACHESIZE`
	local dl=`cat /service/dnscache/env/DATALIMIT`

	echo "Daily cache motion is $dcm"
	echo "CACHESIZE is $cs"
	echo "DATALIMIT is $dl"
}

# fireshow: SHOW iptables FIREwall settings
function fireshow () {
	for tool in iptables ip6tables; do
		$tool --version &>/dev/null || return
		echo -e "\n*** $tool rules ***\n"	
		for tab in filter nat mangle; do
			echo -e "\n***** $tool $tab *****\n"
			sudo $tool -L -nv --line -t $tab
		done
	done
}

# fixperm: FIX file and directory PERMissions to (755, 644)
function fixperm () {
	local ARG=$1;
	if [ "$ARG" ]; then
		echo Resetting permisions for $ARG ...
		find "$ARG" -type d -not -perm 755 -exec chmod -v 755 {} '+'
		find "$ARG" -type f -not -perm 644 -exec chmod -v 644 {} '+'
	else
		echo "Usage: fixperm <dir|file>...";
	fi
}

function force_remove () {
  local args="$@"
  local t0=$(date +%s)
  # NOTE: chmod is required before rm, as some directories could have non-writable permissions.
  find $args -type d -not -perm -700 -exec chmod +700 {} +
  rm -rf $args
  local rmtime=$(($(date +%s) - t0))
  debug "Removed $args (time: $rmtime s)"
}

# fwloglast: FWLOGwatch LAST lines from kernel log
function fwloglast () {
	[ -z "$FWLOGFILE" ] && FWLOGFILE=/var/log/syslog
	[ -r $FWLOGFILE ] || return
	local nlines=$1
	local npackets=$2
	[ "$nlines" != "" ] || nlines=500
	[ "$npackets" != "" ] || npackets=3
	local tmp=`tempfile`
	tail -n $nlines $FWLOGFILE > $tmp
	fwlogwatch -d -s -m $npackets -f $tmp
	rm -rf $tmp
}

# gci: Git Commit Interactive
function gci() {
	git add --interactive $* && git commit $*
}

# gdown: Get something rsynced DOWN
function gdown() {
	local name=$1
	local remote=$2
	[ -z $remote ] && return
	echo rsyncing $name from $remote
	rsync -u $remote:$name $name
}

function git-clone-here () {
    # Clone into existing directory. Method 1.
    local url="$1"
    local dir="${2:-.}"
    [ -e "$dir/.git" ] && { echo "$dir/.git already exists" >&2; return; }
    local tmp=$(mktemp -d)
    git clone "$url" $tmp && mv -iv $tmp/.git $dir && rm -rf $tmp
}

function git-ize () {
    # Clone into existing directory. Method 2.
    local url="$1"
    local dir="${2:-.}"
    local branch=${3:-master}
    [ "$url" ] && [ "$dir" ] || { echo "Usage: git-ize REPO_URL [ DIRECTORY [ BRANCH ] ]" >&2; return; }
    [ -e "$dir/.git" ] && { echo "$dir/.git already exists" >&2; return; }
    ( cd "$dir" &&
        git init -q &&
        git remote add origin "$url" &&
        git fetch -q origin $branch &&
        git reset --mixed origin/$branch
    )
}

# gitll: GIT Long Listing
function gitll () {
	local args="$@"
	gitformat="%h %cs %<(15,trunc)%aN %<(30,trunc)%f"
	git ls-tree -r -t --name-only HEAD $args \
		| xargs -I{} -n 1 -- git log -1 --format="$gitformat {}" {}
}

function git-stat-commits () {
	git shortlog -s -n
}

function git-stat-lines () {
	git ls-files |
	while read f; do git blame -wMCC --line-porcelain "$f" | grep -I '^author '; done |
	sort -f | uniq -ic | sort -nr
}

# google: search args in GOOGLE
function google() {
	local url="http://google.com/search?q=\"$*\""
	if which lynx &>/dev/null ; then
		lynx -cookies "$url"
	elif which w3m &>/dev/null; then
		w3m "$url"
	elif which links &>/dev/null; then
		links "$url"
	else
		echo "Could not find a Web browser."
	fi
}

# groupshare: GROUP SHARE a directory
function groupshare () {
	local ARG=$1
	local GROUP=$2
	if test "$ARG" && test "$GROUP"; then
		echo "Sharing $ARG by adding read/write group access for $GROUP";
		sudo find $ARG -not -group $GROUP -exec chgrp -v $GROUP {} \+
		sudo find $ARG -type d -not -perm -770 -exec chmod -v ug+rw {} \+
		sudo find $ARG -type f -not -perm -660 -exec chmod -v ug+rwx {} \+
	else
		echo "Usage: groupshare <dir|file> <group>";
	fi
}

# gs: Git Status
function gs() {
	git branch | grep \*
	git status --porcelain
}

# gup: Get something rsynced UP
function gup() {
	local name=$1
	local remote=$2
	[ -z $remote ] && return
	[ ! -f $name ] && return
	echo rsyncing $name to $remote
	rsync -u $name $remote:$name
}

function gzip_file () {
  local file=$1
# Skopresuj dany plik do formatu .gz
  local filesize=$(sizeof $file)
  local t0=$(date +%s)
  gzip < $file > $file.gz && rm $file
  local gziptime=$(($(date +%s) - t0))
  local gzipsize=$(sizeof $file.gz)
  debug "Compressed $file from $filesize KB to $gzipsize KB in $gziptime s" INFO
}

# ipsort: IP address sort
function ipsort () {
    sort -t . -g -k1,1 -k2,2 -k3,3 -k4,4
}

# heta: run HEad and TAil on a file
function heta () {
  local arg=$1
  shift 1
  head $@ $arg
  tail $@ $arg
}

# imgfind: find IMaGe-like files using FIND. usage: imgfind /list/of/paths /to/search
function imgfind() {
	find $@ -type f -iregex ".*\.\(jpg\|jpeg\|gif\|bmp\|ppm\|tga\|xbm\|xpm\|tif\|tiff\|png\|pcx\|tga\|wmf\|xcf\)"
}

# imgrep: grep IMaGe-like filenames using GREP
function imgrep() {
	egrep -i "\.(jpg|jpeg|gif|bmp|ppm|tga|xbm|xpm|tif|tiff|png|pcx|tga|wmf|xcf)"
}

# KUBErnetes cluster INFO/status
kubeinfo () {
	echo +++ Kubernetes CLUSTER INFO +++; kubectl cluster-info
	echo +++ Kubernetes NODES +++; kubectl get nodes 2>&1 | grep -v 'duplicate proto type registered'
	echo +++ Kubernetes PODS +++; kubectl get pods -o wide 2>&1 | grep -v 'duplicate proto type registered'
	echo +++ Kubernetes SERVICES +++; kubectl get services -o wide 2>&1 | grep -v 'duplicate proto type registered'
	echo +++ Kubernetes ENDPOINTS +++; kubectl get endpoints 2>&1 | grep -v 'duplicate proto type registered'
	echo +++ Kubernetes SECRETS +++; kubectl get secrets 2>&1 | grep -v 'duplicate proto type registered'
}

function lC () {
	find $@ -mindepth 1 -maxdepth 1 -type d \
		-exec bash -c 'echo -e "$(find "{}" | wc -l)\t{}"' \;
}

# lsofu: LSOF User filter
function lsofu() {
	lsof -u $USER | egrep '(txt|[0-9]+[rwu])\s+(REG|IPv4|DIR)'
}

function lT() {
	ls -ltr $* | tail
}

# lx: ls eXtended, with directories first
function lx() {
	ls -Fhl "$@" | grep --color=never '^d'
	ls -Fhl "$@" | grep --color=never -v '^d\|^t'
}

# lxt: like LX, latest by Time
function lxt() {
	ls -Fhltr $* | grep '^d' | tail;
	ls -Fhltr $* | grep -v '^d\|^t' | tail;
}

# madman: when i am MAD trying to find proper MAN page
function madman() {
	for mandir in `find /opt /usr /var -maxdepth 3 -name man -type d 2>/dev/null`; do
	export MANPATH="$MANPATH:$mandir"
	done
}

function ms2oo () {
	for file in "$@"; do
		if [[ $file =~ \.(doc|docx|rtf)$ ]]; then
			ext=odt
		elif [[ $file =~ (xls|xlsx) ]]; then
			ext=ods
		elif [[ $file =~ 3 ]]; then
			true
		elif [[ $file =~ 4 ]]; then
			true
		elif [[ $file =~ 5 ]]; then
			true
		else
			echo "Unknown file extension" >&2
			return
		fi
		local lopt="--headless --nolockcheck --norestore --convert-to $ext"
		if libreoffice $lopt "$file" &>/dev/null; then
			newname="$( echo $file | perl -pe 's/\.\w{1,10}$//' ).$ext"
			if ! [ -e "$newname" ]; then
				echo "libreoffice did not create $newname" >&2;
			else
				# preserve file modified timestamp
				touch --reference "$file" "$newname"
			fi
		else
			echo "libreoffice returned non-zero status for $file" >&2;
		fi
	done
}

function mysql_via_ssh () {
  local host=$1
  local user=$2
  local mydb=$3
  local myuser=$4
  local mypass=$5
  
  local command="mysql -u$myuser -p$mypass --pager='less -iMSx4 -FX' $dbname"

  if [ $(hostname) == "$host" ] || [ $(hostname -s) == "$host" ]; then
    $command
  else
    if [ -f ssh.conf ]; then
      ssh -F ssh.conf -tt $user@$host "$command"
    elif [ -f .ssh/config ]; then
      ssh -F .ssh/config -tt $user@$host "$command"
    else
      ssh -tt $user@$host "$command"
    fi
  fi
}

function my.strace () {
	strace -v -tt -f -s 8192 -o my.trace $@
}

# stracepid: my favourite invocation of STRACE on a running PID
function my.stracepid () {
	p=$1  # pid
	f=trace.$p  # output file
	t=${2:-86400}  # timeout
	s=${3:-500}  # snap size
	sudo timeout $t strace -v -tt -f -s $s -o $f -p $p
	if test -s $f; then
		echo "strace output saved to this file:"
		ls -lh $f
	else
		rm $f
		echo "strace output empty"
	fi
}

# oo2dir: unpack OpenOffice file into DIRectory
function oo2dir () {
	local doc="$1"
	local dir="$2"
	[ -e "$doc" ] && [ "$dir" ] || {
		echo "Usage: oo2dir <document> <directory>" >&2;
		return
	}
	[ -e "$dir.tmp" ] && rm -rf "$dir.tmp"
	mkdir -p "$dir.tmp"
	if unzip -q "$doc" -d "$dir.tmp"; then
		rsync -a "$dir.tmp/" "$dir/" && rm -rf "$dir.tmp/"
	fi
}

# openport: list OPEN ip PORTs
function openport() {
	echo ">>>>> opened ip ports ( listening/connected/udp) <<<<<"
    # COMMAND     PID            USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
	sudo /usr/bin/lsof -i -n +c15 $@ |
        /bin/grep "LISTEN\|UDP\|ESTABLISHED" |
        /usr/bin/perl -lane 'print "@F[0,2,7,8,9]"' |
        /usr/bin/sort |
        /usr/bin/uniq
}

# perlmissmod: detect PERL MISSing MODules from error message on standard input
function perlmissmod () {
	local tool=${*:-cpan}
	# working:
	# perl -le 'while (<STDIN>) { print qq{$ARGV[0] $1} if /\binstall the (\S+) module/ }' "$tool"
	perl -nle "print q{$tool }.\$1 if /install the (\\S+) module/; END{exit 2}"
}

function perlmissmodinst () {
	local cmd="$@"
	local errfile=`mktemp`
	local tool="sudo cpan -f -i"
	while ! $cmd >/dev/null 2>$errfile; do
		echo "# Command [ $cmd ] failed."
		cat $errfile
		echo "# Trying to fix Perl dependencies."
		sleep 1
		perlmissmod $tool < $errfile | sh -x
		/bin/rm -f $errfile
	done
	echo "# [ $cmd ] ok."
}

# pgdm: PostGresql Database Monitor
function pgdm () {
	local limit=${1:-"5"}
	local interval=${2:-"3"}
	local sql="Select clock_timestamp()-xact_start as xduration, pid, state, query from pg_stat_activity order by xact_start limit $limit"
	watch -n $interval psql -x -q -t -c \""$sql"\" postgres
}

function pgdm0 () {
	local sql="Select clock_timestamp()-xact_start as xduration, procpid, current_query from pg_stat_activity order by xact_start limit 5"
	watch -n 3 psql -x -q -t -c \""$sql"\" postgres
}

# pgpath: Find and export PATH of PostGres binaries.
function pgpath () {
	wanted_versions="`seq 20 -1 10`"
	# Debian convention
	for pgv in $wanted_versions; do
		d="/usr/lib/postgresql/$pgv/bin"
		if [ -d "$d" ] && [ -x "$d/psql" ]; then PATH="$d:$PATH"; break; fi
	done
	# Redhat convention
	for pgv in $wanted_versions; do
		d="/usr/lib/postgresql$pgv/bin"
		if [ -d "$d" ] && [ -x "$d/psql" ]; then PATH="$d:$PATH"; break; fi
	done
	# Local convention: ~/pg13/bin/ ~/pg12/bin/ etc.
	for pgv in $wanted_versions; do
		d="~/pg${pgv}/bin"
		if [ -d "$d" ] && [ -x "$d/psql" ]; then PATH="$d:$PATH"; break; fi
	done
	echo $PATH
	export PATH
}

# pgpaths: Find Postgres installation paths
function pgpaths() {
	local cands="/usr/bin /usr/local/bin"
	for ver in 14 13 12 11 10 9.6 9.5 9.4 9.3 9.2 9.1 9.0; do
		vernodot=`echo $ver | sed 's/\.//'`
		for cand in /usr/lib/postgresql/$ver/bin /opt/postgresql$vernodot/bin; do
			cands="$cands $cand"
		done
	done
	for cand in $cands; do
		if [ -d "$cand" ] && [ -x $cand/psql -o -x $cand/postgres ]; then
			if echo "$PATH" | grep -q -w $cand; then
				echo "$cand already in PATH"
			else
				echo "Adding $cand to PATH"
				export PATH="$PATH:$cand"
			fi
		fi
	done
}

# PGTPS: PostGresql Transactions Per Second
function pgtps () {
	local step=${1:-10}
	test $step -gt 0 || return
	psql -XqAtc '\conninfo' || return
	local ostat=
	while true; do
		stat=$( psql -XqAtc "select xact_commit+xact_rollback from pg_stat_database where datname=current_database()" )
		if test $ostat; then
			local delta=$((stat-ostat))
			local tps=$((delta/step))
			echo $(date --rfc-3339=ns) tps=$tps
		fi
		ostat=$stat
		sleep $step
	done
}

# prx: github PR extended info
function prx() {
	local given="$@"
	if ! [ "$given" ]; then
		given=$(gh pr list --author @me --json number --jq '.[]|.number')
	fi
	local prfields=(additions author baseRefName commits deletions headRefName labels mergeStateStatus number reviewDecision state title updatedAt url)
	local prfieldslst
	IFS=\, eval 'prfieldslst="${prfields[*]}"'
	local prformat='.title + " #" + (.number|tostring) + " (" + .state + ") " + (.labels | map(.name) | join (",")) + "\n"
	+ .author.login + " wants to merge " + (.commits|length|tostring) + " commits into " + .baseRefName + " from " + .headRefName + "\n"
	+ "review: " + .reviewDecision + ", mergeability: " + .mergeStateStatus + ", updated: " + .updatedAt'
	for pr in $given; do
		echo "##### PR $pr #####"
		gh pr view $pr --json $prfieldslst --jq "$prformat"
		# gh pr checks $pr
	done
}

# prc: github PR checks
function prc() {
	gh pr checks "$@"
}

# prd: github PR diff
function prd() {
	gh pr diff "$@"
}

# prl: github PR List
function prl() {
	gh pr list -A@me "$@"
}

# prv: github PR View
function prv() {
	gh pr view "$@"
}

# remote_env_push - PUSH ENVironment to REMOTE
function remote_env_push () {
	(
	cd
	for host in $REMOTE; do
		echo "=== pushing environment to $host ===";
		rsync -Rau .bash_{functions,aliases,variables} .bashrc bin/ .perltidyrc .sql-profiles/ .vimrc .vim $host:
	done
	)
}

# remote_run: RUN command on $REMOTE servers via ssh
function remote_run () {
	local cmd=$@
	local user=${USER:-root}
	for host in $REMOTE; do
		local sshOpt="-q -t -oBatchMode=yes -oConnectTimeout=5"
		echo -n "$host : "
		/usr/bin/ssh $sshOpt $user@$host "$cmd"
	done
	echo
}

# repeat: REPEAT command indefinitely
function repeat() {
	while true; do
	$@;
	echo "sleeping 3 seconds..."
	sleep 3
	done
}

# repo_packages: list apt REPO PACKAGES
function repo_packages () {
	# src: https://tecadmin.net/list-all-packages-available-in-a-repository-on-ubuntu/
	repo=${1:-ppa.launchpad.net}
	grep -h -P -o '^Package: \K.*' /var/lib/apt/lists/${repo}_*_Packages | sort -u
}

# rl: Recent Logtails
function rl() {
	[ "$LOGPATHS" ] || LOGPATHS="/var/log"
	local min=$1
	local num=$2
	[ "$min" != "" ] || min=10
	[ "$num" != "" ] || num=20
	sudo find $LOGPATHS -type f -mmin -$min -ok tail -n $num \{\} \;
}

# rprompt: Red (from trinityos) PROMPT
function rprompt() {
	local RED="\[\033[1;31m\]"
	local GREEN="\[\033[1;32m\]"
	local NO_COL="\[\033[0m\]"
	local WHITE="\[\033[1;37m\]"
	local BLUE="\[\033[1;34m\]"
	local YELLOW="\[\033[1;33m\]"
	local NO_COLOUR="\[\033[0m\]"
	PS1="[\u@\h \W] \`RETVAL=\$?; if [ \$RETVAL -eq 0 ]; then echo -n '$GREEN 0 $NO_COLOUR'; else echo -n '$RED \$RETVAL $NO_COLOUR'; fi\` \$ "
}

# rsync2: RSYNC 2 locations recursively, first #1 to #2, then #2 to #1
function rsync2() {
# This uses rsync in archive mode (-a) and depends on file modification dates (-u).
# The -FF option enables .rsync-filter files.
# The -C option excludes RCS / temporary development files.
	local opts="-u -a -FF -C -z -v"
	rsync $opts "$1" "$2"
	rsync $opts "$2" "$1"
}

function run_with_retry () {
  local maxretry=$1
  shift 1
  local command="$@"
# Run command with configurable number of retries.
  local retry=0
  until bash -c "$command"; do
    status=$?
    let ++retry
    [ $retry -le $maxretry ] \
      && debug "'$command' failed, retrying ($retry)" WARN \
      || { debug "'$command' failed" ERROR; return $status; }
  done
}

# sac: SVN Add & Commit in one shot.
# There can be only one file/directory added at a time.
# File to import should be first, be used in following format:
# sac this/file.txt -m "commit message".
function sac() {
	svn add $1
	svn commit $@
}

# sdif: Subversion DIFf
function sdif() {
	f=`mktemp /tmp/sdif.XXXXXXXXXX`
	svn diff -x "-u -b -w -p --ignore-eol-style" $* > $f
	test -s $f && vim -R $f
	rm $f
}

# settermtitle: just SET TERMinal TITLE, not  sure if that's VTY or XTERM feature?
function settermtitle() {
	echo -ne "\e]2;$@\a\e]1;$@\a";
}

function sizeoffiles () {
# Policz całkowity rozmiar w KB dla danej listy plików lub katalogów.
#  $ du -s -c plik* katalog*
#  4       plik.a
#  8       katalog.b
#  12      total
  du -s -c $@ | tail -n 1 | awk '{print $1}'
}

# sprompt: Standard coloured PROMPT
function sprompt() {
	local NO_COL="\[\033[0m\]"
	local BLUE="\[\033[1;34m\]"
	PS1="<\u@\h> $BLUE\W$NO_COL $ "
}

# sshroot: SSH to given host as ROOT
function sshroot () {
	local args="$@"
	ssh $args -t sudo su - root
}

# spull: Source code PULL
function spull () {
    if [ "$*" ]; then
        args=$@
    else
        args=$(/bin/ls -d *)
    fi
    function _supper() {
        local repo=$1
        local cmd=$2
        echo -n "[$0] Processing [$repo] with [$cmd]... "
        ( cd "$repo" && $cmd )
    }
    for repo in $( echo "$args" | shuf); do
        if ! test -d "$repo"; then
            continue
        elif test -d "$repo/.svn"; then
            _supper "$repo" "svn update"
        elif test -d "$repo/CVS"; then
            _supper "$repo" "cvs update"
        elif test -d "$repo/.git"; then
            _supper "$repo" "git pull"
        fi
    done
}

function tgz_contents_of () {
  local dir=$1
# Kompresuj wszystkie obiekty w katalogu do formatu .tar.gz
  debug "Archiving contents of $dir" INFO
  for obj in $(ls $dir/); do
# Skip files which are already compressed
    [[ "$obj" =~ \.(gz|bz2|xz|lzma|zip)$ ]] && {
      debug "Ignoring already-archived file $obj"; continue; }
    local tarfile="$obj.tar.gz"
    [ -e "$dir/$tarfile" ] && {
      debug "Refusing to overwrite $dir/$tarfile" WARN; continue; }
    size_pre=$(sizeof $dir/$obj)
    local t0=$(date +%s)
    tar -C $dir -c $obj -z -f $dir/$tarfile
    size_post=$(sizeof $dir/$tarfile)
    local tartime=$(($(date +%s) - t0))
    debug "Archived $dir/$obj to .tar.gz (pre: $size_pre KB, post: $size_post KB, time: $tartime s)" INFO
    force_remove $dir/$obj
  done
}

# tonka: prompting loosely based on http://www.faqs.org/docs/Linux-HOWTO/Bash-Prompt-HOWTO.html
function tonka {
	local WHITE="\[\033[1;37m\]"
	local BLUE="\[\033[1;34m\]"
	local YELLOW="\[\033[1;33m\]"
	local NO_COL="\[\033[0m\]"
	TITLEBAR=""
    [[ $TERM =~ xterm|rxvt ]] && TITLEBAR='\[\033]0;\u@\h:\w\007\]'
	local TINFO="$BLUE-($YELLOW\$(date +\"%-d/%-m/%y %H:%M:%S\")$BLUE)-$NO_COL"
    local UINFO="$BLUE-($YELLOW\u$BLUE@$YELLOW\h$BLUE:$YELLOW\w)-$NO_COL"
    local DINFO="$BLUE-($BLUE:$WHITE\\$ $BLUE)-$NO_COL"
	PS0="$YELLOW-$TINFO$YELLOW-\n$NO_COL"
	PS1="$TITLEBAR$YELLOW-$TINFO$UINFO$YELLOW-$NO_COL "
	PS2="$BLUE-$YELLOW-$YELLOW-$NO_COL "
}

function use_public_dns () {
    # overwrite resolv.conf with predefined list of public dbs servers
    local file="~/bin/pubdns.txt"
    [ -f "$file" ] || { echo "missing file: $file" >&2; return; }
    shuf ~/bin/pubdns.txt | head -n 3 | sudo tee /etc/resolv.conf
}

# usrdf: USeR formatted DF
function usrdf () {
    df -k -x squashfs -x tmpfs -x devtmpfs -x overlay -P $@ | perl -mstrict -e '
        my %mount;
        sub as_GB { my $arg = shift; return sprintf "%2d", $arg / 1024 / 1024; }
        while (<>) {
            /^(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)%\s+(\S+)/ or next;
            $mount{$6} = { disk => $1, kbfree => $4, kb => $2 };
        }
        for my $m ( reverse sort { $mount{$a}{kbfree} <=> $mount{$b}{kbfree} } keys %mount ) {
            printf "%s kB (%s GB) of %s kB (%s GB) free in %s (%s).\n", $mount{$m}{kbfree},
                as_GB( $mount{$m}{kbfree} ), $mount{$m}{kb}, as_GB( $mount{$m}{kb} ), $m,
                $mount{$m}{disk};
            $sum     += $mount{$m}{kb};
            $sumfree += $mount{$m}{kbfree};
        }
        print "Total space: " . sprintf( "%2d", $sum / 1024 / 1024 ) . " GB\n";
        print "Free  space: " . sprintf( "%2d", $sumfree / 1024 / 1024 ) . " GB\n";
	'
}

# vcombine: Vertical COMBINE two commands' output
function vcombine ()
{
    # Combine output of multiple commands vertically.
    [ "$1" ] || {
        echo "Usage: vcombine COMMAND ..."
        echo -e "Example:\n  vcombine \"python my.py\" \"ls -tr | tail\""
        return
    } >&2
    local vcargs=()
    local tempfiles=()
    for command do
        local tempfile=$(mktemp /tmp/vcombine.XXXXXXXXXX)
        eval "$command" &> $tempfile
        tempfiles+=($tempfile)
        vcargs+=("$command" $tempfile)
    done
    perl ~/bin/vcombine.pl "${vcargs[@]}"
    rm ${tempfiles[@]}
}

# wunbz2: Wget and UNBZip2 in a pipe
function wunbz2() {
	for url in "$@"; do
	echo wunbz2 $url
	wget -q $url -O - | tar --extract --verbose --bzip2 --file -
	done
}

# wungz: Wget and UNGZip in a pipe
function wungz() {
	for url in "$@"; do
	echo wungz $url
	wget -q $url -O - | tar --extract --verbose --gzip --file -
	done
}

# wunzip: Wget and UNZIP in a pipe
function wunzip() {
	for url in "$@"; do
	echo wunzip $url
	local j=`mktemp`
	wget -q $url -O $j && unzip $j
	rm $j
	done
}

# zview: gunZip and VIEW
function zview() {
	cat "$@" | gunzip -c | vim -R -
}
